﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;

namespace socialweb
{
    public class CultureWordCounts
    {
        public CultureWordCounts(CultureInfo culture)
        {
            Culture = culture;
        }

        public CultureInfo Culture { get; set; }
        public IDictionary<string, int> WordCounts { get; set; } = new ConcurrentDictionary<string, int>(StringComparer.InvariantCultureIgnoreCase);
        public override string ToString()
        {
            return $"{nameof(Culture)}: {Culture.EnglishName}";
        }

        public void AddWords(params string[] words)
        {
            foreach (var word in words)
            {
                if (WordCounts.ContainsKey(word))
                    WordCounts[word] += 1;
                else
                    WordCounts.Add(word, 1);
            }
        }

        public int GetWordMentionsCount(string word)
        {
            var value = 0;
            WordCounts.TryGetValue(word, out value);
            return value;
        }
    }
}