﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;

namespace socialweb
{
    public class WordCountsSummary : KeyedCollection<CultureInfo, CultureWordCounts>
    {
        protected override CultureInfo GetKeyForItem(CultureWordCounts item)
        {
            return item.Culture;
        }

        public IDictionary<string, int> GetWordCounts()
        {
            var dictionaries = this.Items.Select(x => x.WordCounts).ToList();
            var keys = dictionaries.SelectMany(x => x.Keys).Distinct(StringComparer.InvariantCultureIgnoreCase);

            var dictonary= keys.ToDictionary(x => x, key =>
            {
                var sum = 0;
                foreach (var dictionary in dictionaries)
                {
                    int value;
                    if (dictionary.TryGetValue(key, out value))
                        sum += value;
                }
                return sum;
            });

            return dictonary.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value, StringComparer.InvariantCultureIgnoreCase);
        }

        public void WriteWordCountDistributionCsv(FileInfo dest, int skip = 0, int take = 1000)
        {
            var allWords = GetWordCounts().Skip(skip).Take(take).ToDictionary(x=>x.Key, x=> x.Value);
            var cultures = Items.Select(x => x.Culture).ToArray();

            var header = string.Join("|", new[] {"word", "total"}.Concat(cultures.Select(x => x.EnglishName)));

            var lines = new List<string> {header};
            foreach (var word in allWords)
            {
                var cultureWordCounts = cultures.Select(x => this[x].GetWordMentionsCount(word.Key)).ToArray();

                var csvValues = string.Join("|", new[] { word.Key, $"{word.Value:D5}"}.Concat(cultureWordCounts.Select(x => $"{x:D4}")));
                lines.Add(csvValues);
            }

            File.WriteAllLines(dest.FullName,lines);
        }
    }
}