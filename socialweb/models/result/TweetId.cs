namespace socialweb
{
    public class TweetId
    {
        public TweetId(string text, ulong statusId)
        {
            Text = text;
            StatusId = statusId;
        }

        public TweetId()
        {
            
        }
        public string Text { get; set; }
        public ulong StatusId { get; set; }
    }
}