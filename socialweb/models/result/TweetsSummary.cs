﻿using System.Collections.Generic;
using socialweb.models.result;

namespace socialweb
{
    public class TweetsSummary
    {
        public string Keyword { get; set; }
        public int TotalTweetsCount { get; set; }
        public int TotalDistinctTweetsCount { get; set; }
        public ICollection<CultureTweetsSummary> TweetsPerCulture { get; set; }
    }
}