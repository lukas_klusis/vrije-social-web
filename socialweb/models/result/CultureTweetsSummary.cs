﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace socialweb.models.result
{
    public class CultureTweetsSummary
    {
        public string IsoCulture { get; set; }
        public string CultureName { get; set; }
        public int TweetsCount { get; set; }
        public int DistinctTweetsCount { get; set; }
    }
}
