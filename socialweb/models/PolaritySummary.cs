﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace socialweb
{
    public class TextAnalysisPolaritySummary
    {
        public CultureInfo Culture { get; set; }
        public int Neg { get; set; }
        public int Neutral { get; set; }
        public int Pos { get; set; }        
        public int Total => Neg + Neutral + Pos;

        public static string GetHeader()
        {
            return "Total| Neg| Neutral| Pos | Iso Lang | Lang Name";
        }

        public override string ToString()
        {
            return $"{Total:D4} |{Neg:D3}| {Neutral:D3}| {Pos:D3}| {Culture.TwoLetterISOLanguageName}| {Culture.EnglishName}";
        }
    }

    public class PolaritySummary
    {
        public CultureInfo Culture { get; set; }
        public Dictionary<int, int> Polarities { get; set; } = Enumerable.Range(0, 5).ToDictionary(x => x, x => 0);

        public static string GetHeader()
        {
            return "Total| Negative| | Neutral| | Positive | Iso Lang | Lang Name";
        }

        public override string ToString()
        {
            return $"{Polarities.Values.Sum():D4} |" +
                   $"{Polarities[0]:D3}| {Polarities[1]:D3}| {Polarities[2]:D3}| {Polarities[3]:D3}| {Polarities[4]:D3}| {Culture.TwoLetterISOLanguageName}| {Culture.EnglishName}";
        }
    }
}