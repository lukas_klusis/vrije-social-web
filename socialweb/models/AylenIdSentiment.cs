﻿using Aylien.TextApi;

namespace socialweb
{
    public class AylenIdSentiment : IIdSentiment
    {
        public AylenIdSentiment()
        {
            
        }

        public AylenIdSentiment(Sentiment sentiment, ulong statusId)
        {
            Text = sentiment.Text;
            Polarity = sentiment.Polarity;
            PolarityConfidence = sentiment.PolarityConfidence;
            Subjectivity = sentiment.Subjectivity;
            SubjectivityConfidence = sentiment.SubjectivityConfidence;
            StatusId = statusId;
        }

        public double SubjectivityConfidence { get; set; }

        public string Subjectivity { get; set; }

        public double PolarityConfidence { get; set; }

        public string Polarity { get; set; }

        public string Text { get; set; }

        public ulong StatusId { get; set; }
    }

    public interface IIdSentiment
    {
        ulong StatusId { get; set; }
    }

    public class TextProcessingSentiment : IIdSentiment
    {
        public string Label { get; set; }
        public double Neg { get; set; }
        public double Neutral { get; set; }
        public double Pos { get; set; }
        public ulong StatusId { get; set; }
        public string Text { get; set; }
    }
}