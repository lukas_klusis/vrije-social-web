﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using LinqToTwitter;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace socialweb
{
    [TestClass]
    public class TweetsQuery
    {
        [TestMethod]
        public async Task GetTweets()
        {
            var authorizer = new ApplicationOnlyAuthorizer
            {
                CredentialStore = new InMemoryCredentialStore
                {
                    ConsumerKey = "VOFhh2PWW0XC1bmXQ543yvC48",
                    ConsumerSecret = "Am2Sy1rFIitORyxD9lIgZlw4rtQKcAiE9KTa2V7IVPO4KjDUYK"
                }
            };
            await QueryKeywords(authorizer, TestMetadata.Keywords);
        }


        private async Task QueryKeywords(ApplicationOnlyAuthorizer authorizer, params string[] keywords)
        {
            foreach (var keyword in keywords)
                await QueryKeyword(authorizer, keyword);
        }

        private async Task QueryKeyword(ApplicationOnlyAuthorizer authorizer, string keyword)
        {
            var counter = 0;
            try
            {
                var dir = new TestFolders(keyword).GetTwitterFolder();
                var dispatcher = new QueryDispatcher(keyword, authorizer, dir.FullName);

                while (dispatcher.HasNext())
                {
                    counter++;
                    var result = await dispatcher.ExecuteNext();

                    Debug.WriteLine($"{counter:D4}. '{keyword}',"
                                    +
                                    $" Language:{CultureInfo.GetCultureInfo(result.SearchLanguage).EnglishName} ({result.SearchLanguage})"
                                    + $",  result count:{result.Statuses.Count}");
                }
            }
            catch (Exception e)
            {
                for (var i = 0; i < 15; i++)
                {
                    Debug.WriteLine($"Sleeping for 15 minutes; {15 - i} minutes left");
                    Thread.Sleep(TimeSpan.FromMinutes(1));
                }
            }
        }
    }
}