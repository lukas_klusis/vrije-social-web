﻿using System.IO;

namespace socialweb
{
    public class TestFolders
    {
        public string Keyword { get; }
        public DirectoryInfo BaseDir { get; }

        public TestFolders(string keyword, string baseDir = null)
        {
            Keyword = keyword;
            BaseDir = baseDir != null ? new DirectoryInfo(baseDir) : GetDefaultBaseDirectory(keyword);
            if(!BaseDir.Exists) BaseDir.Create();
        }

        public DirectoryInfo GetTwitterFolder() => CreateFolder("tweets");
        public DirectoryInfo GetFilteredTweetsFolder() => CreateFolder("filtered");
        public DirectoryInfo GetTranslationsFolder() => CreateFolder("translated");
        public DirectoryInfo GetSentimentFolder() => CreateFolder("sentiments");
        public DirectoryInfo GetAylienSentimentFolder() => CreateFolder("aylien-sentiments");
        public DirectoryInfo GetTextANalysisSentimentFolder() => CreateFolder("textanalysis-sentiments");
        public DirectoryInfo GetWordCountFolder() => CreateFolder("wordcounts");
        public DirectoryInfo GetSummariesDir() => CreateFolder("summary");

        private DirectoryInfo GetDefaultBaseDirectory(string keyword)
        {
            var currDir = new DirectoryInfo(Directory.GetCurrentDirectory());

            for (var i = 0; i < 2; i++)
                currDir = (currDir.Parent != null) && currDir.Parent.Exists ? currDir.Parent : currDir;

            var path = Path.Combine(currDir.FullName, "data\\" + keyword.CleanFileNamePath());
            var dir = new DirectoryInfo(path);
            if (!dir.Exists) dir.Create();
            return dir;
        }

        private DirectoryInfo CreateFolder(string childDirName)
        {
            var dir = new DirectoryInfo(Path.Combine(BaseDir.FullName, childDirName));
            if (!dir.Exists) dir.Create();
            return dir;
        }
    }
}