using System;
using System.Linq;
using System.Reflection;
using LinqToTwitter;
using Newtonsoft.Json;
using socialweb.models.result;

namespace socialweb
{
    class CustomSearchIdentConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string[]) ||
                   objectType == typeof(Status) ||
                   objectType == typeof(CultureTweetsSummary) ||
                   objectType == typeof(SearchMetaData);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteRawValue(JsonConvert.SerializeObject(value, Formatting.None));
        }
    }

    class NoneIdentConverter : JsonConverter
    {
        private readonly Type[] _types;

        public NoneIdentConverter(params Type[] types)
        {
            _types = types;
        }

        public override bool CanConvert(Type objectType)
        {
            return _types.Contains(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteRawValue(JsonConvert.SerializeObject(value, Formatting.None));
        }
    }
}