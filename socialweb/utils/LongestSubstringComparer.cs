﻿using System.Collections.Generic;
using LinqToTwitter;

namespace socialweb
{
    public class LongestSubstringComparer : IEqualityComparer<string>
    {
        private readonly int _percentToMatch;

        public LongestSubstringComparer(int percentToMatch)
        {
            _percentToMatch = percentToMatch;
        }

        public bool Equals(string x, string y)
            => x.LongestCommonSubstring(y) / ((x.Length + y.Length) / 2.00) >= _percentToMatch / 100.0;

        public int GetHashCode(string obj) => obj.GetHashCode();
    }

    public class LongestTweetSubstringComparer : IEqualityComparer<Status>
    {
        private readonly int _percentToMatch;

        public LongestTweetSubstringComparer(int percentToMatch)
        {
            _percentToMatch = percentToMatch;
        }

        public bool Equals(Status x, Status y)
        => x.Text.LongestCommonSubstring(y.Text) / ((x.Text.Length + y.Text.Length) / 2.0) >= _percentToMatch / 100.0;

        public int GetHashCode(Status obj) => 0;
    }
}