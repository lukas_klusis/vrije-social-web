﻿using System.Collections.Generic;
using System.Globalization;

namespace socialweb
{
    public class IsoTwoLetterCultureEqualityComparere : IEqualityComparer<CultureInfo>
    {
        public bool Equals(CultureInfo x, CultureInfo y) => x.TwoLetterISOLanguageName == y.TwoLetterISOLanguageName;

        public int GetHashCode(CultureInfo obj) => obj.TwoLetterISOLanguageName.GetHashCode();
    }
}