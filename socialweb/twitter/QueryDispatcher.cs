﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LinqToTwitter;
using Newtonsoft.Json;

namespace socialweb
{
    public class QueryDispatcher
    {
        public static HashSet<CultureInfo> Cultures { get; }
        public IDictionary<CultureInfo, CultureQueryDispatcher> CultureDispatchers { get; }

        static QueryDispatcher()
        {
            Cultures = new HashSet<CultureInfo>(new IsoTwoLetterCultureEqualityComparere());
            Cultures.AddRange(CultureInfo.GetCultures(CultureTypes.NeutralCultures)
                .Where(x => x.TwoLetterISOLanguageName.Length == 2));
        }

        public QueryDispatcher(string keyword, ApplicationOnlyAuthorizer authorizer, string directoryPath = null)
        {
            CultureDispatchers = Cultures
                .ToDictionary(x => x,
                    x => new CultureQueryDispatcher(keyword, x, new CultureQueryStore(x, directoryPath), authorizer));
        }

        public bool HasNext() => CultureDispatchers.Any(x => x.Value.HasNext());

        public async Task<Search> ExecuteNext()
        {
            var nextQuery = CultureDispatchers.Values.FirstOrDefault(x => x.HasNext());
            if (nextQuery == null)
                return null;

            return await nextQuery.ExecuteNext();
        }

        public IEnumerable<CultureQueryStore> GetStores()
        {
            return CultureDispatchers.Select(x => x.Value.SearchStore);
        }
    }
}