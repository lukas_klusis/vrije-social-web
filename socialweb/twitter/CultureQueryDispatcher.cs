﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using LinqToTwitter;

namespace socialweb
{
    public class CultureQueryDispatcher
    {
        public string Keyword { get; }
        public CultureInfo Culture { get; }
        public CultureQueryStore SearchStore { get; }
        public ApplicationOnlyAuthorizer Authorizer { get; }

        public CultureQueryDispatcher(string keyword, CultureInfo culture, CultureQueryStore searchStore,
            ApplicationOnlyAuthorizer authorizer)
        {
            Keyword = keyword;
            Culture = culture;
            SearchStore = searchStore;
            Authorizer = authorizer;
        }

        public Search LastSearch => SearchStore.ReadOnlySearches.LastOrDefault();

        public IEnumerable<SearchMetaData> MetaDatas
            => SearchStore.ReadOnlySearches.Select(x => x.SearchMetaData);

        public IEnumerable<Status> Tweets
            => SearchStore.ReadOnlySearches.SelectMany(x => x.Statuses);

        public bool HasNext()
        {
            if (Tweets.Count() > 1500)
                return false;

            return LastSearch == null || LastSearch.Statuses.Count == 100;
        }

        public async Task<Search> ExecuteNext()
        {
            Func<IQueryable<Search>, IQueryable<Search>> nextQuery = GetNextQuery;
            var searchResults = await QueryNextStatuse(nextQuery);
            await SearchStore.AddSearch(searchResults);
            return searchResults;
        }

        private IQueryable<Search> GetNextQuery(IQueryable<Search> search)
        {
            var nextSearch = search
                .Where(x => x.Count == 1500)
                .Where(x => x.Query == Keyword)
                .Where(x => x.Type == SearchType.Search)
                .Where(x=> x.ResultType == ResultType.Mixed)
                .Where(x => x.SearchLanguage == Culture.TwoLetterISOLanguageName);

            if (LastSearch!= null && LastSearch.Statuses.Any())
            {
                var lastId = LastSearch.Statuses.Last().StatusID;
                nextSearch = nextSearch.Where(x => x.MaxID == lastId-1);
            }

            return nextSearch;
        }

        private async Task<Search> QueryNextStatuse(Func<IQueryable<Search>, IQueryable<Search>> search)
        {
            if (string.IsNullOrWhiteSpace(Authorizer.BearerToken))
                await Authorizer.AuthorizeAsync();

            var twitterCtx = new TwitterContext(Authorizer);

            var results = search(twitterCtx.Search)
                .ToList()
                .FirstOrDefault();

            return results;
        }
    }
}