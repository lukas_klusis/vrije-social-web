using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LinqToTwitter;
using Newtonsoft.Json;

namespace socialweb
{
    public class CultureQueryStore
    {
        private readonly AsyncLazy<List<Search>> _searches;
        private readonly Lazy<DirectoryInfo> _directory;

        private List<Search> Searches => _searches.GetAwaiter().GetResult();

        public CultureQueryStore(CultureInfo culture, string directoryPath = null)
        {
            Culture = culture;

            _directory = new Lazy<DirectoryInfo>(() => CreateDirectory(culture, directoryPath));
            _searches = new AsyncLazy<List<Search>>(() => LoadSavedSearches());
            _searches.Start();
        }

        public CultureInfo Culture { get; }
        public IReadOnlyCollection<Search> ReadOnlySearches => Searches.AsReadOnly();
        public DirectoryInfo Directory => _directory.Value;

        private static DirectoryInfo CreateDirectory(CultureInfo culture, string directoryPath = null)
        {
            var dirPath = directoryPath ?? System.IO.Directory.GetCurrentDirectory();
            //dirPath = Path.Combine(dirPath, $"data\\{culture.TwoLetterISOLanguageName}");
            var directory = new DirectoryInfo(dirPath);
            if (!directory.Exists) directory.Create();
            return directory;
        }

        private List<FileInfo> GetSavedSearchFiles()
        {
            return Directory.GetFiles($"{Culture.TwoLetterISOLanguageName}-*.json").ToList();
        }

        private Task<List<int>> GetSavedSearchFileIds()
        {
            return Task.Run(() =>
            {
                var ids = GetSavedSearchFiles().Select(x => x.Name
                        .Replace($"{Culture.TwoLetterISOLanguageName}-", "")
                        .Replace(".json", ""))
                    .Select(Parse.TryParse)
                    .WhereNonNull()
                    .ToList();

                return ids;
            });
        }

        private Task<List<Search>> LoadSavedSearches()
        {
            return Task.Run(() =>
            {
                var files = GetSavedSearchFiles().OrderBy(x => x.Name);

                var searches = files.Select(x =>
                        JsonConvert.DeserializeObject<Search>(File.ReadAllText(x.FullName)))
                    .ToList();

                return searches;
            });
        }

        private async Task<string> GetNextFilePath()
        {
            var currentIds = await GetSavedSearchFileIds();
            var currentMax = currentIds.Any() ? currentIds.Max() +1 : 0;
            return Path.Combine(Directory.FullName, $"{Culture.TwoLetterISOLanguageName}-{currentMax:D5}.json");
        }

        public Task AddSearch(Search search)
        {
            return Task.Run(async () =>
            {
                Searches.Add(search);

                var path = await GetNextFilePath();
                var json = JsonConvert.SerializeObject(search, Formatting.Indented, new CustomSearchIdentConverter());
                File.WriteAllText(path, json);
            });
        }
    }
}