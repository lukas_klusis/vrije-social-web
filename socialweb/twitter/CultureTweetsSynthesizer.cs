﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using LinqToTwitter;

namespace socialweb
{
    public class CultureTweetsSynthesizer
    {
        public string Keyword { get; }
        public CultureInfo Culture { get; }
        private readonly ICollection<Status> _statuses;

        public IReadOnlyCollection<Status> Statuses => _statuses.ToList().AsReadOnly();


        public CultureTweetsSynthesizer(string keyword, CultureInfo culture, ICollection<Status> statuses)
        {
            Keyword = keyword;
            Culture = culture;
            _statuses = statuses;
        }

        private static int _counter = 0;
        public Task<List<Status>> GetDistinctTweets(int skipIfMatchByPercent = 50)
        {
            
            return Task.Run(() =>
            {
                Debug.WriteLine($"{_counter++:D5}. Calculating distinct tweets: {Culture.EnglishName} ({Culture.TwoLetterISOLanguageName}), total tweets: {_statuses.Select(x => x.Text).Count()}");
                var result =_statuses.Distinct(new LongestTweetSubstringComparer(skipIfMatchByPercent)).ToList();
                Debug.WriteLine($"{_counter++:D5}. Done calculating distinct tweets: {Culture.EnglishName} ({Culture.TwoLetterISOLanguageName}), total tweets: {_statuses.Select(x => x.Text).Count()}, Distinct: {result.Select(x=> x.Text).Count()}");
                return result;
            });
        }
    }
}