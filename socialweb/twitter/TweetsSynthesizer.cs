﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using LinqToTwitter;
using socialweb.models.result;

namespace socialweb
{
    public class TweetsSynthesizer
    {
        private readonly string _keyword;
        private readonly ICollection<CultureTweetsSynthesizer> _cultureTweets;

        private Dictionary<CultureInfo, List<Status>> _distinctTweetsCache;

        private TweetsSummary _summaryCache;

        public TweetsSynthesizer(string keyword, ICollection<CultureQueryStore> stores)
        {
            _keyword = keyword;
            _cultureTweets =
                stores.Select(
                    x =>
                        new CultureTweetsSynthesizer(keyword, x.Culture,
                            x.ReadOnlySearches.SelectMany(s => s.Statuses).ToList())).ToList();
        }

        public Dictionary<CultureInfo, List<Status>> GetTweets()
        {
            return _cultureTweets.ToDictionary(x => x.Culture, x => x.Statuses.ToList());
        }

        public async Task<Dictionary<CultureInfo, List<Status>>> GetDistinctTweets(int skipIfMatchByPercent = 50)
        {
            if (_distinctTweetsCache != null)
                return _distinctTweetsCache;

            var tasks = _cultureTweets.ToDictionary(x => x.Culture, x => x.GetDistinctTweets());
            await Task.WhenAll(tasks.Values);

            _distinctTweetsCache = tasks.ToDictionary(x => x.Key, x => x.Value.Result);
            return _distinctTweetsCache;
        }

        public async Task<TweetsSummary> GetSummary(int skipIfMatchByPercent = 50)
        {
            if (_summaryCache != null)
                return _summaryCache;

            var distinctTweets = await GetDistinctTweets(skipIfMatchByPercent);

            var cultureTweetsSummary = distinctTweets.Select(x => new CultureTweetsSummary
            {
                CultureName = x.Key.EnglishName,
                IsoCulture = x.Key.TwoLetterISOLanguageName,
                DistinctTweetsCount = x.Value.Count,
                TweetsCount = _cultureTweets.First(c => c.Culture == x.Key).Statuses.Count
            }).ToList();

            _summaryCache = new TweetsSummary
            {
                Keyword = _keyword,
                TotalTweetsCount = cultureTweetsSummary.Sum(x => x.TweetsCount),
                TotalDistinctTweetsCount = cultureTweetsSummary.Sum(x => x.DistinctTweetsCount),
                TweetsPerCulture = cultureTweetsSummary
            };

            return _summaryCache;
        }

        public async Task<Dictionary<CultureInfo, List<TweetId>>> GetTweetsTexts()
        {
            var distinctTweets = await GetDistinctTweets();

            return distinctTweets.ToDictionary(x => x.Key, x => x.Value.Select(s => new TweetId(s.Text, s.StatusID)).ToList());
        }

        
    }
}