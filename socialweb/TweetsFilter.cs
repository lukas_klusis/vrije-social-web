﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LinqToTwitter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace socialweb
{
    [TestClass]
    public class TweetsFilter
    {
        [TestMethod]
        public async Task GetTweetTexts()
        {
            foreach (var keyword in TestMetadata.Keywords)
            {
                var synthesizer = GetSynthesizer(keyword);
                var texts = await synthesizer.GetTweetsTexts();

                var destDir = new TestFolders(keyword).GetFilteredTweetsFolder();
                foreach (var tweets in texts)
                {
                    var filePath = Path.Combine(destDir.FullName, $"{tweets.Key.TwoLetterISOLanguageName}.txt");
                    File.WriteAllText(filePath, JsonConvert.SerializeObject(tweets.Value, Formatting.Indented,new NoneIdentConverter(typeof(TweetId))));
                }

                var tweetsSummary = await synthesizer.GetSummary();
                var summaryFilePath = Path.Combine(destDir.FullName, "sumamry.json");
                File.WriteAllText(summaryFilePath, JsonConvert.SerializeObject(tweetsSummary, Formatting.Indented, new CustomSearchIdentConverter()));
            }
        }


        public TweetsSynthesizer GetSynthesizer(string keyword)
        {
            var sourceDir = new TestFolders(keyword).GetTwitterFolder();
            var dispatcher = new QueryDispatcher(keyword, new ApplicationOnlyAuthorizer(), sourceDir.FullName);
            var stores = dispatcher.CultureDispatchers.Values.Select(x => x.SearchStore).ToList();
            var nonEmptyStores = stores.Where(x => x.ReadOnlySearches.SelectMany(s => s.Statuses).Any()).ToList();

            nonEmptyStores.ForEach(
                x =>
                    x.ReadOnlySearches.SelectMany(search => search.Statuses)
                        .ToList()
                        .ForEach(status => status.Text = status.Text.ReplaceUrl("")));

            return new TweetsSynthesizer(keyword, nonEmptyStores);
        }
    }
}