using System.Collections.Generic;

namespace socialweb
{
    public static class HashSetExtensions
    {
        public static void AddRange<T>(this HashSet<T> set, IEnumerable<T> collection)
        {
            foreach (T item in collection)
            {
                set.Add(item);
            }
        }
    }
}