using System.Collections.Generic;
using System.Linq;

namespace socialweb
{
    public static class Parse
    {
        public static int? TryParse(string @string)
        {
            int result;
            return int.TryParse(@string, out result) ? (int?) result : null;
        }

        public static IEnumerable<T> WhereNonNull<T>(this IEnumerable<T?> collection)
            where T: struct
        {
            return collection.Where(x => x.HasValue).Select(x => x.Value);
        }
    }
}