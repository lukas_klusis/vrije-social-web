﻿using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace socialweb
{
    [TestClass]
    public class WordCounts
    {
        [TestMethod]
        public void Summarize()
        {
            foreach (var keyword in TestMetadata.Keywords)
            {
                var folders = new TestFolders(keyword);
                var sourceDir = folders.GetTranslationsFolder();
                var dstDist = folders.GetSummariesDir();

                var summary = GetWordsCountSummary(sourceDir);
                
                summary.WriteWordCountDistributionCsv(new FileInfo(Path.Combine(dstDist.FullName,"wordcounts.csv")));
            }
        }

        private WordCountsSummary GetWordsCountSummary(DirectoryInfo directory)
        {
            var sumamry = new WordCountsSummary();

            foreach (var file in directory.GetFiles("*.txt"))
            {
                var tweets = TranslatedTweetsParser.ParseTweets(file);
                var culture= new CultureInfo(file.Name.Replace(".txt",""));

                var words = tweets.SelectMany(x =>
                    {
                        var text = x.Text;
                        Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                        text = rgx.Replace(text, "");
                        return text
                            .Split(' ')
                            .Where(w => w.Length > 2);

                    }).ToArray();

                var wordCounts = new CultureWordCounts(culture);

                wordCounts.AddWords(words);

                sumamry.Add(wordCounts);
            }
            return sumamry;
        }
    }
}