﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Aylien.TextApi;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace socialweb
{
    [TestClass]
    public class TweetsSentiment
    {
        [TestMethod]
        public async Task CalculateSentiments()
        {
            foreach (var keyword in TestMetadata.Keywords)
            {
                var folders = new TestFolders(keyword);
                var sourceDir = folders.GetTranslationsFolder();
                var destDir = folders.GetSentimentFolder();
                await CalculateSentiments(sourceDir.FullName, destDir.FullName);
            }
        }

        [TestMethod]
        public void CalculateAylienSentiments()
        {
            foreach (var keyword in TestMetadata.Keywords)
            {
                var folders = new TestFolders(keyword);
                var sourceDir = folders.GetTranslationsFolder();
                var destDir = folders.GetAylienSentimentFolder();

                var clients = new List<Client>
                {
                    new Client("7640c8a2 ", "c68bd85b87ee951f4dc9a91f011dcf09"),
                    new Client("8a17a79e ", "842cba0e37b3b21a4da6a7f66f9b8e3b"),
                    new Client("2ed5c1b2", "f9bee7d2629b7e47deaa7398e71614ab"),
                    new Client("5735a0da ", "94aaa481c51a98ed691f474de068332a"),
                };

                Func<ICollection<TweetId>, IEnumerable<AylenIdSentiment>> processor = toProcess =>
                {
                    var tasks = toProcess.Zip(clients, (x, y) => new
                        {
                            Tweet = x,
                            Client = y,
                        }).Select(async x => await Task.Run(() =>
                        {
                            var sentiment = x.Client.Sentiment(text: x.Tweet.Text, language: "en", mode: "tweet",
                                url: @"https://api.aylien.com/api/v1/sentiment");
                            return new AylenIdSentiment(sentiment, x.Tweet.StatusId);
                        }))
                        .ToArray();

                    var sentiments = Task.WhenAll(tasks).GetAwaiter().GetResult();
                    return sentiments;
                };

                CalculateSentiments(sourceDir.FullName, destDir.FullName, clients.Count,processor);
            }
        }


        [TestMethod]
        public void CalculateTextAnalysisSentiments()
        {
            foreach (var keyword in TestMetadata.Keywords)
            {
                var folders = new TestFolders(keyword);
                var sourceDir = folders.GetTranslationsFolder();
                var destDir = folders.GetTextANalysisSentimentFolder();

                Func<ICollection<TweetId>, IEnumerable<TextProcessingSentiment>> processor = toProcess =>
                {
                    var tasks = new List<Task<TextProcessingSentiment>>();
                    foreach (var tweetId in toProcess)
                    {
                        var task = Task.Run(async () =>
                        {
                            using (var client = new HttpClient())
                            {
                                var url = "http://text-processing.com/api/sentiment/";

                                var text = tweetId.Text; // Regex.Replace(tweetId.Text, @"[^\u0000-\u007F]+", string.Empty);

                                var content = new FormUrlEncodedContent(new[]
                                {
                                    new KeyValuePair<string, string>("text", text),
                                });

                                var response = await client.PostAsync(url, content);
                                var responseString = await response.Content.ReadAsStringAsync();

                                dynamic sentimentJson = JsonConvert.DeserializeObject(responseString);

                                return new TextProcessingSentiment
                                {
                                    StatusId = tweetId.StatusId,
                                    Label = sentimentJson.label,
                                    Neg = sentimentJson.probability.neg,
                                    Neutral = sentimentJson.probability.neutral,
                                    Pos= sentimentJson.probability.pos,
                                    Text = tweetId.Text
                                };
                            }
                        });
                        tasks.Add(task);
                    }
                    
                    var sentiments = Task.WhenAll(tasks).GetAwaiter().GetResult();
                    return sentiments;
                };

                CalculateSentiments(sourceDir.FullName, destDir.FullName, 8, processor);
            }
        }

        private void CalculateSentiments<T>(string sourceDir, string destDir, int chunckSize, Func<ICollection<TweetId>, IEnumerable<T>> processor)
            where T : IIdSentiment
        {


            var totalCallCounter = 0;
            var directory = new DirectoryInfo(sourceDir);
            foreach (var file in directory.GetFiles())
            {
                var destPath = Path.Combine(destDir, file.Name);
                var processesSentiments = ParseSentiments<T>(destPath);
                var processedTweetsIds = processesSentiments.Select(x => x.StatusId).ToList();

                var tweetsToProcess =
                    TranslatedTweetsParser.ParseTweets(file)
                        .Where(x => !processedTweetsIds.Contains(x.StatusId))
                        .ToList();


                int callCounter = 0;
                while (tweetsToProcess.Count > 0)
                {
                    try
                    {
                        var toProcess = tweetsToProcess.Take(chunckSize).ToList();
                        var sentiments = processor(toProcess);
                        callCounter = callCounter + chunckSize;
                        processesSentiments.AddRange(sentiments);
                        tweetsToProcess = tweetsToProcess.Skip(chunckSize).ToList();
                    }
                    catch (Aylien.TextApi.Error e)
                    {
                        Debug.WriteLine($"Sleeping for 15 seconds, {e.Message}");
                        Thread.Sleep(TimeSpan.FromSeconds(15));
                    }
                    catch (System.Exception e)
                    {
                        Debug.WriteLine($"{file.Name} was not completed, sleeping for 10 seconds");
                        Thread.Sleep(TimeSpan.FromSeconds(10));
                    }
                    Debug.WriteLine($"{file.Name.Replace(".txt", "")}: Call counter: {callCounter}");
                }

                totalCallCounter += callCounter;
                Debug.WriteLine($"Saving file {file.Name}: Total call counter: {totalCallCounter}");

                var json = JsonConvert.SerializeObject(processesSentiments, Formatting.Indented);
                File.WriteAllText(destPath, json);
            }
        }

        private
            List<T> ParseSentiments<T>(string path)
        {
            if (!File.Exists(path))
                return new List<T>();

            var json = File.ReadAllText(path);
            if (string.IsNullOrWhiteSpace(json))
                return new List<T>();

            try
            {
                return JsonConvert.DeserializeObject<List<T>>(json);
            }
            catch (Exception)
            {
                return new List<T>();
            }
        }


        private async Task CalculateSentiments(string sourceDir, string destDir)
        {
            var directory = new DirectoryInfo(sourceDir);

            foreach (var file in directory.GetFiles())
            {
                var tweets = TranslatedTweetsParser.ParseTweets(file);
                var destPath = Path.Combine(destDir, file.Name);
                var obj = new
                {
                    Data = tweets
                };

                var jsonSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Formatting = Formatting.Indented,
                };

                var json = JsonConvert.SerializeObject(obj, jsonSettings);

                using (var client = new HttpClient())
                {
                    var url = "http://www.sentiment140.com/api/bulkClassifyJson?appid=lukas.klusis@student.uva.nl";
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(url, content);
                    var responseString = await response.Content.ReadAsStringAsync();
                    File.WriteAllText(destPath, responseString);
                }
            }
        }
    }

    public static class TranslatedTweetsParser
    {
        public static IList<TweetId> ParseTweets(FileInfo file)
        {
            var lines = File.ReadAllLines(file.FullName);
            var tweets = new List<TweetId>();

            for (int i = 0; i < lines.Length; i++)
            {
                var line = lines[i].Trim(' ', '	', '"');
                var idString = Regex.Match(line, "(?<=StatusId.*)\\d{18}").Value;

                if (string.IsNullOrWhiteSpace(idString))
                {
                    lines[i] = "";
                    continue;
                }

                var id = ulong.Parse(idString);
                line = line.Replace(idString, "");
                line = Regex.Replace(line, "(\"\\s{0,5}StatusId\\s{0,5}\"\\s{0,5}:)", "");
                line = Regex.Replace(line, "(.{0,10}\"Text\":.{0,10}(\"))", "");
                line = line
                    .Replace("{", "")
                    .Replace("}", "")
                    .Replace("\",", "")
                    .Replace(",\"", "");

                lines[i] = line;

                tweets.Add(new TweetId(lines[i], id));
            }
            return tweets.Where(x=> !string.IsNullOrWhiteSpace(x.Text)).ToList() ;
        }
    }
}