﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace socialweb
{
    [TestClass]
    public class SummarizeSentiments
    {
        [TestMethod]
        public void SummarizeTextAnalysisSentiments()
        {
            foreach (var keyword in TestMetadata.Keywords)
            {
                var dir = new TestFolders(keyword).GetTextANalysisSentimentFolder();
                SummarizeTextAnalysisSentimentsAndSave(dir);
            }
        }

        private void SummarizeTextAnalysisSentimentsAndSave(DirectoryInfo folder)
        {
            var summaries = new List<TextAnalysisPolaritySummary>();
            foreach (var file in folder.GetFiles("*.txt"))
            {
                var culture = new CultureInfo(file.Name.Replace(".txt", ""));
                var summary = new TextAnalysisPolaritySummary { Culture = culture };

                var sentiments = JsonConvert.DeserializeObject<IEnumerable<TextProcessingSentiment>>(File.ReadAllText(file.FullName));
                foreach (var sentiment in sentiments)
                {
                    switch (sentiment.Label)
                    {
                        case "neg":
                            summary.Neg++;
                            break;
                        case "pos":
                            summary.Pos++;
                            break;
                        case "neutral":
                            summary.Neutral++;
                            break;
                    }
                }
                summaries.Add(summary);
            }

            var resPath = Path.Combine(folder.FullName, "sentiments.csv");
            var csv = TextAnalysisPolaritySummary.GetHeader();
            csv += "\r\n" + string.Join("\r\n", summaries);
            File.WriteAllText(resPath, csv);
        }


        [TestMethod]
        public void Summarize()
        {
            foreach (var keyword in TestMetadata.Keywords)
            {
                var dir = new TestFolders(keyword).GetSentimentFolder();
                SummarizeAndSave(dir);
            }
        }

        private void SummarizeAndSave(DirectoryInfo folder)
        {
            var summaries = new List<PolaritySummary>();

            foreach (var file in folder.GetFiles("*.txt"))
            {
                Dictionary<int, int> polarities = Enumerable.Range(0, 5).ToDictionary(x => x, x => 0);
                var culture = new CultureInfo(file.Name.Replace(".txt", ""));

                summaries.Add(new PolaritySummary
                {
                    Culture = culture,
                    Polarities = polarities
                });

                var json = File.ReadAllText(file.FullName);
                dynamic results = JObject.Parse(json);
                results = results.data;
                foreach (var result in results)
                {
                    int polarity = result.polarity;
                    polarities[polarity]++;
                }
            }

            var resPath = Path.Combine(folder.FullName, "sentiments.csv");
            var csv = PolaritySummary.GetHeader();
            csv += "\r\n" + string.Join("\r\n", summaries);
            File.WriteAllText(resPath, csv);
        }
    }
}