﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace socialweb
{

    public class CountryLanguagePercentange
    {
        public string Language { get; set; }
        public double Percentage { get; set; }

        public CountryLanguagePercentange() { }

        public CountryLanguagePercentange(string language, double percentage)
        {
            Language = language;
            Percentage = percentage;
        }

        public override string ToString()
        {
            return $"{nameof(Language)}: {Language}, {nameof(Percentage)}: {Percentage}";
        }
    }

    public class CountryLanguage
    {
        private readonly string _languages;
        public string Country { get; set; }

        public static string[] KnownLanguages { get; set; }
        public CountryLanguage(string country, string languages)
        {
            _languages = languages;
            Country = country;
        }

        public CountryLanguagePercentange[] GetLanguages()
        {
            var pattern = new Regex(@" \d*\.*\d*%");
            var languagePercentagePairs = _languages
                .Split(',')
                .Select(x => new
                {
                    Language = pattern.Replace(x, "").Trim(),
                    Percentage = pattern.Match(x).Success ? (double?)double.Parse(pattern.Match(x).Value.Replace("%", "")) : null
                }).ToList();

            var knownPercentage = languagePercentagePairs.Sum(x => x.Percentage ?? 0);
            var leftPercentage = 100 - knownPercentage;

            var languagesWithoutPercentage = languagePercentagePairs.Count(x => x.Percentage == null);

            var result = languagePercentagePairs.Select(x => new CountryLanguagePercentange(x.Language, x.Percentage ?? leftPercentage / languagesWithoutPercentage))
                .ToArray();
            return result;
        }

        public CountryLanguagePercentange[] GetFilteredLanguages()
        {
            var languages = GetLanguages().Select(x => new
            {
                MatchingKnownLanguage = KnownLanguages.FirstOrDefault(kn => x.Language.ToLower().Contains(kn.ToLower())),
                CountryLanguagePercentange = x
            }).Select(x => new CountryLanguagePercentange(x.MatchingKnownLanguage, x.CountryLanguagePercentange.Percentage))
            .Where(x => x.Language != null)
            .ToArray();
            return languages;
        }

        public override string ToString()
        {
            return $"{nameof(Country)}: {Country}, Languages: {string.Join(",", GetFilteredLanguages().Select(x => x.ToString()))}";
        }
    }

    [TestClass]
    public class LanguagesByCountry
    {

       

        [TestMethod]
        public void AgregrateLanguages()
        {
            var knownLangues = languagesWithSufficientTweets.Replace("\r\n", "|").Split('|').ToArray();

            var a = languages.Replace("\r\n", "♣").Split('♣').Select(x => new
            {
                Country = x.Split('|')[0],
                Languages = x.Split('|')[1],
                LanguagesWords = x.Split('|')[1].Split(' ').Select(w => w.Trim()).ToList(),
            }).Where(x => x.LanguagesWords.Any(w => knownLangues.Contains(w, StringComparer.InvariantCultureIgnoreCase)))
            .ToList();
            //--------------------------------------------
            CountryLanguage.KnownLanguages = knownLangues;

            var b = a.Select(x => new CountryLanguage(x.Country, x.Languages)).ToList();


            var csvLines = b.SelectMany(x => x.GetFilteredLanguages().Select(l => $"{x.Country}|{l.Language}|{l.Percentage}")).ToList();

            var csv = string.Join("\r\n", csvLines);
        }


        private string languages =
    @"Afghanistan|Afghan Persian or Dari  50%, Pashto  35%, Turkic languages  11%, 30 minor languages  4%, much bilingualism, but Dari functions as the lingua franca
Akrotiri|English, Greek
Albania|Albanian 98.8% , Greek 0.5%,
Algeria|Arabic , French , Berber or Tamazight ; dialects include Kabyle Berber , Shawiya Berber , Mzab Berber, Tuareg Berber
American Samoa|Samoan 88.6% , English 3.9%, Tongan 2.7%,
Andorra|Catalan , French, Castilian, Portuguese
Angola|Portuguese 71.2% , Umbundu 23%, Kikongo 8.2%, Kimbundu 7.8%, Chokwe 6.5%, Nhaneca 3.4%, Nganguela 3.1%, Fiote 2.4%, Kwanhama 2.3%, Muhumbi 2.1%, Luvale 1%,
Anguilla|English
Antigua and Barbuda|English , Antiguan creole
Argentina|Spanish , Italian, English, German, French, indigenous
Armenia|Armenian  97.9%, Kurdish  1%,
Aruba|Papiamento   69.4%, Spanish 13.7%, English  7.1%, Dutch  6.1%, Chinese 1.5%,
Australia|English 76.8%, Mandarin 1.6%, Italian 1.4%, Arabic 1.3%, Greek 1.2%, Cantonese 1.2%, Vietnamese 1.1%,
Austria|German  88.6%, Turkish 2.3%, Serbian 2.2%, Croatian  1.6%,
Azerbaijan|Azerbaijani   92.5%, Russian 1.4%, Armenian 1.4%,
Bahamas, The|English , Creole
Bahrain|Arabic , English, Farsi, Urdu
Bangladesh|Bangla 98.8% ,
Barbados|English , Bajan
Belarus|Russian  70.2%, Belarusian  23.4%,
Belgium|Dutch  60%, French  40%, German  less than 1%
Belize|English 62.9% , Spanish 56.6%, Creole 44.6%, Maya 10.5%, German 3.2%, Garifuna 2.9%,
Benin|French , Fon and Yoruba , tribal languages
Bermuda|English , Portuguese
Bhutan|Sharchhopka 28%, Dzongkha  24%, Lhotshamkha 22%,
Bolivia|Spanish  60.7%, Quechua  21.2%, Aymara  14.6%, foreign languages 2.4%, Guarani  0.6%,
Bosnia and Herzegovina|Bosnian  52.9%, Serbian  30.8%, Croatian  14.6%,
Botswana|Setswana 77.3%, Sekalanga 7.4%, Shekgalagadi 3.4%, English  2.8%, Zezuru/Shona 2%, Sesarwa 1.7%, Sembukushu 1.6%, Ndebele 1%,
Brazil|Portuguese
British Virgin Islands|English
Brunei|Malay , English, Chinese dialects
Bulgaria|Bulgarian  76.8%, Turkish 8.2%, Roma 3.8%,
Burkina Faso|French , native African languages belonging to Sudanic family spoken by 90% of the population
Burma|Burmese
Burundi|Kirundi 29.7% , Kirundi and
Cabo Verde|Portuguese , Crioulo
Cambodia|Khmer  96.3%,
Cameroon|24 major African language groups, English , French
Canada|English  58.7%, French  22%, Punjabi 1.4%, Italian 1.3%, Spanish 1.3%, German 1.3%, Cantonese 1.2%, Tagalog 1.2%, Arabic 1.1%,
Cayman Islands|English  90.9%, Spanish 4%, Filipino 3.3%,
Central African Republic|French , Sangho , tribal languages
Chad|French , Arabic , Sara , more than 120 different languages and dialects
Chile|Spanish 99.5% , English 10.2%, indigenous 1% ,
China|Standard Chinese or Mandarin
Christmas Island|English , Chinese, Malay
Cocos (Keeling) Islands|Malay , English
Colombia|Spanish
Comoros|Arabic , French , Shikomoro
Congo, Democratic Republic of the|French , Lingala , Kingwana , Kikongo, Tshiluba
Congo, Republic of the|French , Lingala and Monokutuba , many local languages and dialects
Cook Islands|English  86.4%, Cook Islands Maori   76.2%,
Costa Rica|Spanish , English
Cote d'Ivoire|French , 60 native dialects of which Dioula is the most widely spoken
Croatia|Croatian  95.6%, Serbian 1.2%,
Cuba|Spanish
Curacao|Papiamento   81.2%, Dutch  8%, Spanish 4%, English  2.9%,
Cyprus|Greek  80.9%, Turkish  0.2%, English 4.1%, Romanian 2.9%, Russian 2.5%, Bulgarian 2.2%, Arabic 1.2%, Filipino 1.1%,
Czechia|Czech  95.4%, Slovak 1.6%,
Denmark|Danish, Faroese, Greenlandic , German
Dhekelia|English, Greek
Djibouti|French , Arabic , Somali, Afar
Dominica|English , French patois
Dominican Republic|Spanish
Ecuador|Spanish  93% , Quechua 4.1%,
Egypt|Arabic , English and French widely understood by educated classes
El Salvador|Spanish , Nawat
Equatorial Guinea|Spanish  67.6%,
Eritrea|Tigrinya , Arabic , English , Tigre, Kunama, Afar,
Estonia|Estonian  68.5%, Russian 29.6%, Ukrainian 0.6%,
Ethiopia|Oromo  33.8%, Amharic  29.3%, Somali  6.2%, Tigrigna   5.9%, Sidamo 4%, Wolaytta 2.2%, Gurage 2%, Afar  1.7%, Hadiyya 1.7%, Gamo 1.5%, Gedeo 1.3%, Opuuo 1.2%, Kafa 1.1%,
Falkland Islands (Islas Malvinas)|English 89%, Spanish 7.7%,
Faroe Islands|Faroese 93.8% , Danish 3.2%,
Fiji|English , Fijian , Hindustani
Finland|Finnish  89%, Swedish  5.3%, Russian 1.3%,
France|French  100%, rapidly declining regional dialects and languages
French Polynesia|French  61.1%, Polynesian  31.4%, Asian languages 1.2%,
Gabon|French , Fang, Myene, Nzebi, Bapounou/Eschira, Bandjabi
Gambia, The|English , Mandinka, Wolof, Fula,
Gaza Strip|Arabic, Hebrew , English
Georgia|Georgian  87.6%, Azeri 6.2%, Armenian 3.9%, Russian 1.2%,
Germany|German
Ghana|Asante 16%, Ewe 14%, Fante 11.6%, Boron  4.9%, Dagomba 4.4%, Dangme 4.2%, Dagarte  3.9%, Kokomba 3.5%, Akyem 3.2%, Ga 3.1%,
Gibraltar|English , Spanish, Italian, Portuguese
Greece|Greek  99%,
Greenland|Greenlandic  , Danish , English
Grenada|English , French patois
Guam|English 43.6%, Filipino 21.2%, Chamorro 17.8%,
Guatemala|Spanish  60%, Amerindian languages 40%
Guernsey|English, French, Norman-French dialect spoken in country districts
Guinea|French
Guinea-Bissau|Crioulo 90.4%, Portuguese 27.1% , French 5.1%, English 2.9%,
Guyana|English , Guyanese Creole, Amerindian languages , Indian languages , Chinese
Haiti|French , Creole
Holy See (Vatican City)|Italian, Latin, French, various
Honduras|Spanish , Amerindian dialects
Hong Kong|Cantonese  89.5%, English  3.5%, Mandarin  1.4%,
Hungary|Hungarian  99.6%, English 16%, German 11.2%, Russian 1.6%, Romanian 1.3%, French 1.2%,
Iceland|Icelandic, English, Nordic languages, German widely spoken
India|Hindi 41%, Bengali 8.1%, Telugu 7.2%, Marathi 7%, Tamil 5.9%, Urdu 5%, Gujarati 4.5%, Kannada 3.7%, Malayalam 3.2%, Oriya 3.2%, Punjabi 2.8%, Assamese 1.3%, Maithili 1.2%,
Indonesia|Bahasa Indonesia , English, Dutch, local dialects
Iran|Persian , Azeri Turkic and Turkic dialects, Kurdish, Gilaki and Mazandarani, Luri, Balochi, Arabic,
Iraq|Arabic , Kurdish , Turkmen  and Assyrian  are official in areas where they constitute a majority of the population), Armenian
Ireland|English , Irish
Isle of Man|English, Manx Gaelic
Israel|Hebrew , Arabic , English
Italy|Italian , German , French , Slovene
Jamaica|English, English patois
Japan|Japanese
Jersey|English 94.5% , Portuguese 4.6%,
Jordan|Arabic , English
Kazakhstan|Kazakh  74% , Russian  94.4%
Kenya|English , Kiswahili , numerous indigenous languages
Kiribati|I-Kiribati, English
Korea, North|Korean
Korea, South|Korean, English
Kosovo|Albanian  94.5%, Bosnian 1.7%, Serbian  1.6%, Turkish 1.1%,
Kuwait|Arabic , English widely spoken
Kyrgyzstan|Kyrgyz  71.4%, Uzbek 14.4%, Russian  9%,
Laos|Lao , French, English, various ethnic languages
Latvia|Latvian  56.3%, Russian 33.8%,
Lebanon|Arabic , French, English, Armenian
Lesotho|Sesotho  , English , Zulu, Xhosa
Liberia|English 20% , some 20 ethnic group languages few of which can be written or used in correspondence
Libya|Arabic , Italian, English ; Berber
Liechtenstein|German 94.5%  , Italian 1.1%,
Lithuania|Lithuanian  82%, Russian 8%, Polish 5.6%,
Luxembourg|Luxembourgish 88.8%, French 4.2%, Portuguese 2.3%, German 1.1%
Macau|Cantonese 83.3%, Mandarin 5%, Hokkien 3.7%, English 2.3%,
Macedonia|Macedonian  66.5%, Albanian  25.1%, Turkish 3.5%, Roma 1.9%, Serbian 1.2%,
Madagascar|French , Malagasy , English
Malawi|English , Chichewa , Chinyanja, Chiyao, Chitumbuka, Chilomwe, Chinkhonde, Chingoni, Chisena, Chitonga, Chinyakyusa, Chilambya
Malaysia|Bahasa Malaysia , English, Chinese , Tamil, Telugu, Malayalam, Panjabi, Thai
Maldives|Dhivehi , English
Mali|French , Bambara 46.3%, Peul/Foulfoulbe 9.4%, Dogon 7.2%, Maraka/Soninke 6.4%, Malinke 5.6%, Sonrhai/Djerma 5.6%, Minianka 4.3%, Tamacheq 3.5%, Senoufo 2.6%, Bobo 2.1%, unspecified 0.7%,
Malta|Maltese  90.1%, English  6%, multilingual 3%,
Marshall Islands|Marshallese  98.2%,
Mauritania|Arabic , Pulaar, Soninke, Wolof , French
Mauritius|Creole 86.5%, Bhojpuri 5.3%, French 4.1%, two languages 1.4%,
Mexico|Spanish only 92.7%, Spanish and indigenous languages 5.7%, indigenous only 0.8%, unspecified 0.8%
Micronesia, Federated States of|English , Chuukese, Kosrean, Pohnpeian, Yapese, Ulithian, Woleaian, Nukuoro, Kapingamarangi
Moldova|Moldovan 58.8% , Romanian 16.4%, Russian 16%, Ukrainian 3.8%, Gagauz 3.1% , Bulgarian 1.1%,
Monaco|French , English, Italian, Monegasque
Mongolia|Khalkha Mongol 90% , Turkic, Russian
Montenegro|Serbian 42.9%, Montenegrin  37%, Bosnian 5.3%, Albanian 5.3%, Serbo-Croat 2%,
Montserrat|English
Morocco|Arabic , Berber languages , Tachelhit, Tarifit), French
Mozambique|Emakhuwa 25.3%, Portuguese  10.7%, Xichangana 10.3%, Cisena 7.5%, Elomwe 7%, Echuwabo 5.1%,
Namibia|Oshiwambo languages 48.9%, Nama/Damara 11.3%, Afrikaans 10.4% , Otjiherero languages 8.6%, Kavango languages 8.5%, Caprivi languages 4.8%, English  3.4%,
Nauru|Nauruan 93% , English 2% ,
Nepal|Nepali  44.6%, Maithali 11.7%, Bhojpuri 6%, Tharu 5.8%, Tamang 5.1%, Newar 3.2%, Magar 3%, Bajjika 3%, Urdu 2.6%, Avadhi 1.9%, Limbu 1.3%, Gurung 1.2%,
Netherlands|Dutch
New Caledonia|French , 33 Melanesian-Polynesian dialects
New Zealand|English  89.8%, Maori  3.5%, Samoan 2%, Hindi 1.6%, French 1.2%, Northern Chinese 1.2%, Yue 1%,
Nicaragua|Spanish  95.3%, Miskito 2.2%, Mestizo of the Caribbean coast 2%,
Niger|French , Hausa, Djerma
Nigeria|English , Hausa, Yoruba, Igbo , Fulani, over 500 additional indigenous languages
Niue|Niuean  46% , Niuean and English 32%, English  11%, Niuean and
Norfolk Island|English  67.6%,
Northern Mariana Islands|Philippine languages 32.8%, Chamorro  24.1%, English  17%,
Norway|Bokmal Norwegian , Nynorsk Norwegian , small Sami- and Finnish-speaking minorities
Oman|Arabic , English, Baluchi, Urdu, Indian dialects
Pakistan|Punjabi 48%, Sindhi 12%, Saraiki  10%, Pashto  8%, Urdu  8%, Balochi 3%, Hindko 2%, Brahui 1%, English , Burushaski, and
Palau|Palauan  66.6%, Carolinian 0.7%,
Panama|Spanish , indigenous languages , Buglere, Kuna, Embera, Wounaan, Naso , and Bri Bri), Panamanian English Creole , English, Chinese , Arabic, French Creole,
Papua New Guinea|Tok Pisin , English , Hiri Motu , some 836 indigenous languages spoken ; most languages have fewer than 1,000 speakers
Paraguay|Spanish , Guarani
Peru|Spanish  84.1%, Quechua  13%, Aymara  1.7%, Ashaninka 0.3%,
Philippines|Filipino  and English ; eight major dialects - Tagalog, Cebuano, Ilocano, Hiligaynon or Ilonggo, Bicol, Waray, Pampango, and Pangasinan
Pitcairn Islands|English , Pitkern
Poland|Polish  98.2%, Silesian 1.4%,
Portugal|Portuguese , Mirandese
Puerto Rico|Spanish, English
Qatar|Arabic , English commonly used as a second language
Romania|Romanian  85.4%, Hungarian 6.3%, Romany  1.2%,
Russia|Russian  85.7%, Tatar 3.2%, Chechen 1%,
Rwanda|Kinyarwanda only  93.2%, Kinyarwanda and
Saint Barthelemy|French , English
Saint Helena, Ascension, and Tristan da Cunha|English
Saint Kitts and Nevis|English
Saint Lucia|English , French patois
Saint Martin|French , English, Dutch, French Patois, Spanish, Papiamento
Saint Pierre and Miquelon|French
Saint Vincent and the Grenadines|English, French patois
Samoa|Samoan  , English
San Marino|Italian
Sao Tome and Principe|Portuguese 98.4% , Forro 36.2%, Cabo Verdian 8.5%, French 6.8%, Angolar 6.6%, English 4.9%, Lunguie 1%,
Saudi Arabia|Arabic
Senegal|French , Wolof, Pulaar, Jola, Mandinka
Serbia|Serbian  88.1%, Hungarian 3.4%, Bosnian 1.9%, Romany 1.4%,
Seychelles|Seychellois Creole  89.1%, English  5.1%, French  0.7%,
Sierra Leone|English , Mende , Temne , Krio
Singapore|Mandarin  36.3%, English  29.8%, Malay  11.9%, Hokkien 8.1%, Cantonese 4.1%, Tamil  3.2%, Teochew 3.2%,
Sint Maarten|English  67.5%, Spanish 12.9%, Creole 8.2%, Dutch  4.2%, Papiamento  2.2%, French 1.5%,
Slovakia|Slovak  78.6%, Hungarian 9.4%, Roma 2.3%, Ruthenian 1%,
Slovenia|Slovenian  91.1%, Serbo-Croatian 4.5%,
Solomon Islands|Melanesian pidgin , English , 120 indigenous languages
Somalia|Somali , Arabic , Italian, English
South Africa|IsiZulu  22.7%, IsiXhosa  16%, Afrikaans  13.5%, English  9.6%, Sepedi  9.1%, Setswana  8%, Sesotho  7.6%, Xitsonga  4.5%, siSwati  2.5%, Tshivenda  2.4%, isiNdebele  2.1%, sign language 0.5%,
South Sudan|English , Arabic , regional languages include Dinka, Nuer, Bari, Zande, Shilluk
Spain|Castilian Spanish  74%, Catalan ) 17%, Galician  7%, Basque  2%, Aranese  along with Catalan; <5,000 speakers)
Sri Lanka|Sinhala  74%, Tamil  18%,
Sudan|Arabic , English , Nubian, Ta Bedawie, Fur
Suriname|Dutch , English , Sranang Tongo , Caribbean Hindustani , Javanese
Svalbard|Norwegian, Russian
Swaziland|English , siSwati
Sweden|Swedish , small Sami- and Finnish-speaking minorities
Switzerland|German  63.5%, French  22.5%, Italian  8.1%, English 4.4%, Portuguese 3.4%, Albanian 3.1%, Serbo-Croatian 2.5%, Spanish 2.2%, Romansch  0.5%,
Syria|Arabic , Kurdish, Armenian, Aramaic, Circassian, French, English
Taiwan|Mandarin Chinese , Taiwanese , Hakka dialects
Tajikistan|Tajik , Russian widely used in government and business
Tanzania|Kiswahili or Swahili , Kiunguja , English , Arabic , many local languages
Thailand|Thai  90.7%, Burmese 1.3%,
Timor-Leste|Tetum , Portuguese , Indonesian, English
Togo|French , Ewe and Mina , Kabye  and Dagomba
Tokelau|Tokelauan 93.5% , English 58.9%, Samoan 45.5%, Tuvaluan 11.6%, Kiribati 2.7%,
Tonga|English and Tongan 87%, Tongan  10.7%, English  1.2%,
Trinidad and Tobago|English , Caribbean Hindustani , French, Spanish, Chinese
Tunisia|Arabic , French , Berber
Turkey|Turkish , Kurdish,
Turkmenistan|Turkmen  72%, Russian 12%, Uzbek 9%,
Turks and Caicos Islands|English
Tuvalu|Tuvaluan , English , Samoan, Kiribati
Uganda|English , Ganda or Luganda ,
Ukraine|Ukrainian  67.5%, Russian  29.6%,
United Arab Emirates|Arabic , Persian, English, Hindi, Urdu
United Kingdom|English
United States|English 79.2%, Spanish 12.9%,
Uruguay|Spanish , Portunol, Brazilero
Uzbekistan|Uzbek  74.3%, Russian 14.2%, Tajik 4.4%,
Venezuela|Spanish
Vietnam|Vietnamese, English, French, Chinese, Khmer
Virgin Islands|English 71.6%, Spanish 17.2%, French 8.6%,
Wallis and Futuna|Wallisian  58.9%, Futunian 30.1%, French  10.8%,
West Bank|Arabic, Hebrew , English
Western Sahara|Arabic
Yemen|Arabic
Zimbabwe|Shona , Ndebele , English";


        private string languagesWithSufficientTweets =
            @"German
English
Spanish
French
Italian
Portuguese
Russian
Arabic
Dutch
Thai
Indonesian
Turkish
Korean
Swedish
Czech
Chinese
Polish
Danish
Finnish
Japanese
Hungarian
Vietnamese
Greek
Romanian
Lithuanian
Bulgarian
Basque
Norwegian Bokmål
Norwegian Nynorsk
Estonian
Persian
Icelandic
Slovenian
Latvian
Ukrainian
Welsh
Hebrew
Armenian
Burmese
Sanskrit";

    }
}
